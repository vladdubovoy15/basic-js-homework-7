"use strict";

const arr = ["Milan", "Moscow", "Kiev", ["Kharkiv", "Odessa"], "Lviv", ["Paris", "Marcel"], "Athens", "London"];
const parent = document.getElementById("parent");
const list = document.createElement("ul");
list.style.listStyle = "none";

function createList(array, div) {
       div.append(list);
       list.innerHTML = array.map(el => {
              if(typeof el === "object") {
                 return `<li>
                               <ul>${el.map(element => `<li>${element}</li>`).join("")}</ul>
                        </li>`
              } else {return `<li>${el}</li>`}; 
       }).join(""); 
};
createList(arr, parent);